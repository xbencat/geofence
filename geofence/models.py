from django.db import models
from django.contrib.postgres.fields import ArrayField


class WayPoint(models.Model):
    """
    example of a row from dataset:
    1,Sushi Mushi,"[48.1298533, 17.0938053]"
    """
    name = models.TextField()
    coordinates = models.JSONField()
