import json
from pyproj import Geod
from django.shortcuts import render
from django.http import JsonResponse
from geofence.models import WayPoint
from shapely.geometry import Point, LineString
from shapely.ops import nearest_points


def index(request):
    return render(request, 'index.html')


def search(request):
    all_entries = WayPoint.objects.values('name', 'coordinates')
    json_data = json.loads(request.body)     # path and radius

    res = list()

    # user selected path
    line = LineString([tuple(p) for p in json_data['path']])

    # Geometry transform function
    geod = Geod(ellps="WGS84")

    # cycle all points from
    for way_point in all_entries:
        p = Point(tuple(way_point['coordinates']))

        # calculate distance between path and point
        dist = geod.geometry_length(LineString(nearest_points(line, p)))

        if dist <= float(json_data['radius']):
            res.append({
                "name": way_point['name'],
                "coordinates": way_point['coordinates']
            })

    return JsonResponse(res, safe=False)
