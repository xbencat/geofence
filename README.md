## Geofence

**Application description**: 

Application for searching nearest(specified radius) Restaurants and Fast Foods around selected path.

**Data source**: [Open Street Maps](https://www.openstreetmap.org/)

**Technologies used**: [Django](https://www.djangoproject.com/), [PostgreSQL](https://www.postgresql.org/), Javascript, [Leaflet](https://leafletjs.com/)  

**Documentation**: in `documentation.md`