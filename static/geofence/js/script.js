let map = L.map('map', {
    center: [48.148598,17.107748],
    zoom: 15
});

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© OpenStreetMap'
}).addTo(map);

// VARIABLES
let markers = L.layerGroup().addTo(map);
let lines = L.layerGroup().addTo(map);
let restaurants = L.layerGroup().addTo(map);
let search = document.getElementById('search-form');
let iconOptions = {iconUrl: '/static/images/restaurant-icon.png',iconAnchor: [20, 41]};
let customIcon = L.icon(iconOptions);
let markRestaurant = {
    clickable: true,
    draggable: true,
    icon: customIcon
};
let searchInput = document.getElementById("search-input");
let sideTable = document.getElementById('side-table');
let sideTableBody = sideTable.getElementsByTagName('tbody')[0];
let csrftoken = getCookie('csrftoken');
let lastPoint = {};
let path = [];


// EVENTS
map.addEventListener('click', onMapClick);
search.addEventListener('submit', onSearchClick);
searchInput.addEventListener('keyup', searchTable);


// FUNCTIONS
function onSearchClick(e) {
    e.preventDefault();
    let searchRadius = document.getElementById('search-radius').value;
    markers.clearLayers();
    lines.clearLayers();
    searchPoints(searchRadius).then();
    lastPoint = {};
    path = [];
}
function onMapClick(e) {
    restaurants.clearLayers();
    searchInput.style.display = 'none'; // hide search bar for restaurants
    searchInput.value = ""; // clear input

    while (sideTableBody.firstChild) {
        sideTableBody.removeChild(sideTableBody.firstChild);
    }


    L.marker(e.latlng).addTo(markers);

    if(isEmpty(lastPoint) === true){
        lastPoint = e.latlng;
        // push initial point to path
        path.push(Object.values(e.latlng));
    } else {
        // when we already have one point start drawing
        drawLine(lastPoint, e.latlng);
    }
}

function drawLine(first, second) {
    let points = Array();
    points.push(first);
    points.push(second);
    lastPoint = second;
    // push second point to path
    path.push(Object.values(second));

    L.polyline(points, {color: 'blue'}).addTo(lines);
}

function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

function searchTable(){
    let i, td, tr, txtValue, filter;
    tr = sideTable.getElementsByTagName("tr");
    filter = searchInput.value.toUpperCase()

    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

async function searchPoints(radius) {

    let params = {
        "path": path,
        "radius": radius
    }
    let response = await fetch('/geofence/search/', {
        method: 'post',
        headers: {'Content-Type': 'application/json','Origin':'http://127.0.0.1:8000/', "X-CSRFToken":csrftoken},
        body: JSON.stringify(params)
    });

    if (response.ok) {
        searchInput.style.display = 'block'; // show search bar for restaurant
        let re_json = await response.json();
        for (let i = 0; i < re_json.length ; i++) {
            // create markers and table
            L.marker(re_json[i]["coordinates"],markRestaurant).addTo(restaurants);
            let row =  sideTableBody.insertRow();
            row.innerHTML = "<td>"+parseInt(i+1)+"</td><td>"+re_json[i]["name"]+"</td>";

        }
    } else {
        alert("HTTP-Error: " + response.status);
    }
}


function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i].trim();

            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

