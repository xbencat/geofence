# Install
`pip install requirements.txt`

`python manage.py makemigrations geofence`

`python manage.py migrate geofence`

*populate database with some data*

run:

`python manage.py`

# Overview

This application shows  Restaurants or Fast Foods in Bratislava around selected path in specified radius.


This is it in action:

*Restaurants/Fast foods around path in radius:*

path:

![Screenshot](static/images/path.png)

result:

![Screenshot](static/images/result.png)

The application has 2 separate parts, the client which is a [frontend web application](#frontend) using [Leaflet](https://leafletjs.com/) and the [backend application](#backend) written in [Django](https://www.djangoproject.com/), backed by PostgreSQL.

# Frontend

Frontend is a static HTML page (`index.html`), which shows a leaflet.js widget. 

All relevant frontend code is in `/static/geofence/js/script.js` which is referenced from `templates/index.html`. Frontend code calls api via javascripts fetch api and displays features on map and sidebar.


# Backend
Backend is written in Django and is responsible for querying geo data and responding with results.

## Data
Data is downloaded from [Open Street Maps](https://www.openstreetmap.org/). I used only restaurants and fast foods in and around Bratislava. Data is stored in table called geofence_waypoint.

Table structure:

![Screenshot](static/images/geofence_waypoint.png)

## Api

**Search around path**

`POST geofence/search/`
*request:*
```json
{
  "path":[[48.14913756559802,17.10988053074817],[48.14759136790455,17.10717623771051]],
  "radius":"60"
}
```
*response:*
```json
[
  { "name": "Gourmet bistro", 
    "coordinates": [48.1477292, 17.1083321]}, 
  { "name": "Ganesh Utsav", 
    "coordinates": [48.1482439, 17.1091833]} 
]
```
